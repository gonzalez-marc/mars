import React from "react";
import Coords from "../atoms/Coords";

export enum CellFloor {
    SAND,
    GRAVEL
}

export interface CellModel {
    x: number;
    y: number;
    type: CellFloor;
}

const Cell: React.FC<CellModel> = (props) => {
    const {x,y,type} = props;
    return (<div className='border square' key={`${x};${y}`}><Coords x={x} y={y}/></div>);
}

export default Cell;
