import React from "react";
import Cell, {CellFloor, CellModel} from "../molecules/Cell";

interface GridModel {
    width: number;
    height: number;
}

const Grid: React.FC<GridModel> = (props) => {
    const {height, width} = props;
    const board: CellModel[] = Array(width * height)
        .fill(0)
        .map((_, i) => ({
            x: i % width,
            y: height - 1 - Math.floor(i / width),
            type: Math.floor(Math.random() * 2) ? CellFloor.SAND : CellFloor.GRAVEL,
        }));
    console.log(board);
    return (
        <div className={`grid grid-cols-${width} grid-3d`}>
            {board.map((cell) => (<Cell {...cell}/>))}
        </div>
    );
}

export default Grid;
