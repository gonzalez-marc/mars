import React from "react";

interface CoordsModel {
    x: number;
    y: number;
}

const Coords: React.FC<CoordsModel> = (props) => {
    const {x, y} = props;
    return (<span>{x};{y}</span>);
}

export default Coords;
