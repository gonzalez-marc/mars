module.exports = {
purge: ['./src/**/*.{js,jsx,ts,tsx}', './public/index.html'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      gridTemplateColumns:{
        10:'repeat(10, 60px)'
      }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
